export PREFIX=/opt/local
export TOPDIR=$(CURDIR)
export BUILD_CC=gcc
export WGET=wget
export HOST=arm-armv5te-linux-gnueabi
export ARCH=arm
export CROSS_COMPILE=$(HOST)-
export CC=${CROSS_COMPILE}gcc
export CXX=${CROSS_COMPILE}g++
export LD=${CROSS_COMPILE}ld
export STRIP=${CROSS_COMPILE}strip
export AR=${CROSS_COMPILE}ar
export RANLIB=${CROSS_COMPILE}ranlib
export PKGSUFFIX:=-kirkwood
export SYSROOT:=/opt/x-tools/arm-armv5te-linux-gnueabi/arm-armv5te-linux-gnueabi/sysroot
export PKG_CONFIG_PATH=$(CURDIR)/rootfs/lib/pkgconfig

